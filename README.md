Copyright (c) 2016-2017 thomas.zink _at_ uni-konstanz _dot_ de

Usage of the works is permitted provided that this instrument is retained with the works, so that any entity that uses the works is notified of this instrument.  
DISCLAIMER: THE WORKS ARE WITHOUT WARRANTY. 

# btrfs-snapshot

Create readonly btrfs snapshots tagged with a date format.
It allows you to easily script interval based, recycling btrfs snapshots.

This script is a wrapper for the commands `btrfs subvolume snapshot` and `date`.
It simply passes the `FORMAT` argument to `date` and uses the return string 
to name the snapshot. Basically, the script executes the command:

 	$ btrfs subvolume snapshot -r <source> <dest>/$(basename <source>)-$(date $FORMAT)

However, it also performs some error checking. If the destination subvolume already
exists, it will be removed prior to creating a new one.

## Usage

    usage: btrfs-snapshot.sh [-f <format>] <-f source> <-d dest> [-r <receive>]

    Options:
        -f format:	 the date format used to name the snapshot. See 'man date'. Default YYmmdd.
        -s source:	 the subvolume to create a snapshot from.
        -d dest:	 the destination directory where to create the snapshot.
        -r receive:	 send incremental snapshots to receive directory on different device.

### Examples

1. Create a snapshot of the subvolume `/media/data` for the current day of the month 
named `/media/data-XX-YY`, where `XX` is the number of day (01-31) and `YY` is the
localized abbreviated name of the day (eg. `Mon` to `Sun`):

	btrfs-snapshot.sh -f +%d-%a -s /media/data -d /media

2. Create a snapshot of the subvolume `/media/data` for the current week of the year
named `/media/data-XX-week`, where `XX` is the number of the current week (01-53):

	btrfs-snapshot.sh +%W-week /media/backups /media

3. Create a snapshot of subvolume `/media/data` named `/media/data-YYmmdd` and 
send it to a backup disk on /backups:

    btrfs-snapshot.sh -s /media/data -d /media -r /backups

### Install and automate snapshots

The easiest way is to clone the directory to `/opt`, then adjust and install the crontab
`btrfs-snapshot-cron` to `/etc/cron.d`.

	cd opt
	git clone https://gitlab.com/teezee/btrfs-snapshot.git 
	chmod +x /opt/btrfs-snapshot/btrfs-snapshot.sh
	cp /opt/btrfs-snapshot/btrfs-snapshot-cron /etc/cron.d

