#!/bin/bash
###
# Copyright (c) 2016-2017 thomas.zink _at_ uni-konstanz _dot_ de
# Usage of the works is permitted provided that this instrument is retained with the works, so that any entity that uses the works is notified of this instrument.
# DISCLAIMER: THE WORKS ARE WITHOUT WARRANTY. 
###
# btrfs-snapshot
# Create readonly btrfs snapshots tagged with a date format.
#
# This script is a wrapper for the commands `btrfs subvolume snapshot` and `date`.
# It simply passes the `FORMAT` argument to `date` and uses the return string 
# to name the snapshot. Basically, the script executes the command:
#
# 	$ btrfs subvolume snapshot -r <source> <dest>/<source>_snapshot_`date $FORMAT`
#
# However, it also performs some error checking. If the destination subvolume already
# exists, it will be removed prior to creating a new one.
# This allows you to easily script interval based, recycling btrfs snapshots.
####

# error handling
set -euf -o pipefail
shopt -s failglob inherit_errexit

ME=$(basename "$0")
LOGGER="logger -i"
# check if we have busybox logger (no -i)
[[ -L $(command -v logger) ]] && LOGGER="logger";

usage() {
    echo "usage: $ME [-f <format>] <-f source> <-d dest> [-r <receive>]";
	echo;
	echo "Options:";
	printf "\t-f format:\t the date format used to name the snapshot. See 'man date'. Default YYmmdd.\n";
	printf "\t-s source:\t the subvolume to create a snapshot from.\n"
	printf "\t-d dest:\t the destination directory where to create the snapshot.\n"
    printf "\t-r receive:\t send incremental snapshots to receive directory on different device.\n"
	exit 0;
}

src=""
dst=""
format="+%Y%m%d"
receive=""

while getopts ":hs:d:f:r:" opt; do
	case "${opt}" in
		s)  [ ! -d "${OPTARG}" ] && { echo "ERROR: ${OPTARG} not a directory"; usage; }
			src="${OPTARG}"
			;;
		d)  [ ! -d "${OPTARG}" ] && { echo "ERROR: ${OPTARG} not a directory"; usage; }
			dst="${OPTARG}"
			;;
		f)	[ -z "${OPTARG}" ] &&  { echo "WARN: format ${OPTARG} empty"; usage; }
			format="${OPTARG}"
			;;
        r)  [ ! -d "${OPTARG}" ] && { echo "ERROR: ${OPTARG} not a directory"; usage; }
            receive="${OPTARG}"
            ;;
		h)	usage; exit 1;;
		*)	usage; exit 1;;
	esac
done
shift $((OPTIND-1))

# check for correct arguments
[[ -z "$format" ]] || [[ -z "$src" ]] || [[ -z "$dst" ]] && usage;
[[ ! -d "$src" ]] && echo "ERROR: directory $src not found!" && usage;
[[ ! -d "$dst" ]] && echo "ERROR: directory $dst not found!" && usage;

# find snapshot basename
base="$(basename "${src}")";

# set snapshot name suffix
suf="$(date "${format}")"

# set snapshot dir
shot="${dst}/${base}-${suf}"
parent=""

# check if snapshot exists and delete
if [[ -d "${shot}" ]]; then
	btrfs subvolume delete "${shot}"
	$LOGGER -t "${ME}" -s "btrfs subvolume delete ${shot}"
fi

# create snapshot
btrfs subvolume snapshot -r "${src}" "${shot}"
$LOGGER -t "${ME}" -s "btrfs subvolume snapshot -r ${src} ${shot}"
if [[ -x "${dst}/current" ]]; then
    parent="$(readlink -f "${dst}"/current)"
    rm "${dst}/current"
fi
ln -s "${base}-${suf}" "${dst}/current"

# if we have a receive dir, send the snapshot, when possible incrementally
if [[ -d "${receive}" ]]; then
    if [[ -z "${parent}" ]]; then
        btrfs send "${shot}" | btrfs receive "${receive}"
        $LOGGER -t "${ME}" -s " btrfs send ${shot} | btrfs receive ${receive}"
    else
        btrfs send -p "${parent}" "${shot}" | btrfs receive "${receive}"
        $LOGGER -t "${ME}" -s " btrfs send -p ${parent} ${shot} | btrfs receive ${receive}"
    fi
fi
